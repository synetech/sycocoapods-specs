#
#  Be sure to run `pod spec lint SYNBaseProject.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "SYNBase"
  s.version      = "1.0.6"
  s.summary      = "A base project that probivies a unfied functionality to all our projects"
  s.description  = <<-DESC
                    This project provides a base functions, structures and approaches that all our new projects depend on.
                    DESC
  s.homepage     = "https://bitbucket.org/synetech/base_ios"
  s.license      = "MIT"
  s.author       = { "Daniel Rutkovsky" => "daniel.rutkovsky@synetech.cz" }
  
  s.source       = { :git => "git@bitbucket.org:synetech/base_ios.git", :tag => "#{s.version}" }
  # s.source_files  = "Source",
  
  s.requires_arc = true
  s.platform     = :ios, "9.0"

  s.subspec 'DependencyInjection' do |ss|
    ss.source_files = 'Source/DependencyInjection/**/*.swift'
    ss.dependency 'Swinject', '~> 2.0'
  end

  s.subspec 'Routing' do |ss|
    ss.source_files = 'Source/Routing/**/*.swift'
  end

  s.subspec 'VIPER' do |ss|
    ss.source_files = 'Source/VIPER/**/*.swift'
    ss.exclude_files = 'Source/VIPER/SVProgressHUD'

    ss.subspec 'SVProgressHUD' do |sss|
      sss.source_files = 'Source/VIPER/**/*.swift'
      sss.dependency 'SVProgressHUD', '~> 2.1'
    end
    
  end

end
