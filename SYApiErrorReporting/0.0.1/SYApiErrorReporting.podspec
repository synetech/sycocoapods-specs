Pod::Spec.new do |s|
  s.name         = 'SYApiErrorReporting'
  s.version      = '0.0.1'
  s.summary      = 'A library that provides a template to API reporting.'
  # s.license      = 'PRIVATE'
  s.author             = { 'Daniel Rutkovský' => 'daniel.rutkovsky@synetech.cz' }
  s.social_media_url   = 'http://www.synetech.cz'
  s.platform     = :ios, '8.0'
  s.homepage     = 'http://www.synetech.cz'
  s.source       = { :git => 'https://bitbucket.org/synetech/ios-api-error-reporting.git', :tag => s.version.to_s }
  s.source_files  = 'SYApiErrorReporting/**/*.swift'
  s.requires_arc = true
end
