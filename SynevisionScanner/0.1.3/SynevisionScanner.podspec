Pod::Spec.new do |s|
  s.name              = "SynevisionScanner"
  s.version           = "0.1.3"
  s.summary           = "Synevision scanner pod for ORIFLAME app."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/oriflame-synevision-scanner-ios.git", :tag => s.version.to_s }
  s.resources        = ['Resources/*.xcassets', 'Resources/Fonts/*', 'Localized/*.lproj/*.strings', 'Resources/KOS/*.csv', 'Resources/KOS/Models.scnassets']
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'

  s.dependency 'FontBlaster'
  s.dependency 'Result'
  s.dependency 'RxCocoa'
  s.dependency 'SteviaLayout'
  s.dependency 'Swinject'
  s.dependency 'SynetechVision'
  s.dependency 'SYNBase/Scalable'
  s.dependency 'Translations'
  s.dependency 'Kingfisher'
end
