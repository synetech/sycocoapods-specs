Pod::Spec.new do |s|
  s.name              = "SynevisionScanner"
  s.version           = "0.0.1"
  s.summary           = "Synevision scanner pod for ORIFLAME app."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/oriflame-synevision-scanner-ios.git", :tag => s.version.to_s }
  s.resource_bundles  = {'SynevisionScanner' => ['Resources/*']}
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '5.0'
end
