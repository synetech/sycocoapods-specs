Pod::Spec.new do |s|
  s.name              = "CommonNetworking"
  s.version           = "0.0.5"
  s.summary           = "Library for Oriflame networking that is not suitable in other more specific networking libraries."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
	s.source            = { :git => "https://bitbucket.org/synetech/oriflame-common-networking-ios.git/", :tag => s.version.to_s }
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'

  s.dependency 'OriflameConfig'
  s.dependency "Moya", '12.0.1'
  s.dependency "Moya/RxSwift", '12.0.1'
  s.dependency "RxSwift", '4.5'
end
