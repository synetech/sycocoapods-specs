Pod::Spec.new do |s|
  s.name              = "SyneDynamickej"
  s.version           = "0.1.0"
  s.summary           = "Synetech dynamic screen(Patent pending)"
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/dynamic-screens-ios.git", :tag => s.version.to_s }
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "10.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'

  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'ObjectMapper'
  s.dependency 'SteviaLayout'
  s.dependency 'Kingfisher'
end
