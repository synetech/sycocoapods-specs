Pod::Spec.new do |s|
  s.name         = 'ORLocalizedStrings'
  s.version      = '1.0.2'
  s.summary      = 'A library that provides access to localized string for Oriflame apps.'
  s.license      = "MIT"
  s.author             = { 'Daniel Rutkovsky' => 'daniel.rutkovsky@synetech.cz' }
  s.social_media_url   = 'http://www.synetech.cz'
  s.platform     = :ios, '9.0'
  s.homepage     = 'http://www.synetech.cz'
  s.source       = { :git => 'git@bitbucket.org:synetech/oriflame-translations-ios.git', :tag => s.version.to_s }
  s.source_files  = 'ORLocalizedStrings/Oriflame Localized Strings/**/*.swift'
  s.requires_arc = true
  s.dependency  'Alamofire'
  s.dependency  'ObjectMapper', '~> 3.3'
  s.dependency  'SYApiErrorReporting'
end
