Pod::Spec.new do |s|
  s.name         = 'ORLocalizedStrings'
  s.version      = '0.5.3'
  s.summary      = 'A library that provides access to localized string for Oriflame apps.'
  # s.license      = 'PRIVATE'
  s.author             = { 'Daniel Rutkovsky' => 'daniel.rutkovsky@synetech.cz' }
  s.social_media_url   = 'http://www.synetech.cz'
  s.platform     = :ios, '8.0'
  s.homepage     = 'http://www.synetech.cz'
  s.source       = { :git => 'https://bitbucket.org/synetech/oriflame-translations-ios.git', :tag => s.version.to_s }
  s.source_files  = 'ORLocalizedStrings/Oriflame Localized Strings/**/*.swift'
  s.requires_arc = true
  s.dependency  'Alamofire', '~> 4.4'
  s.dependency  'AFDateHelper', '~> 4.2.1'
  s.dependency  'ObjectMapper', '~> 2.2'
end
