Pod::Spec.new do |s|
  s.name         = 'ORLocalizedStrings'
  s.version      = '0.2.5'
  s.summary      = 'A library that provides access to localized string for Oriflame apps.'
  # s.license      = 'PRIVATE'
  s.author             = { 'Daniel Rutkovský' => 'daniel.rutkovsky@synetech.cz' }
  s.social_media_url   = 'http://www.synetech.cz'
  s.platform     = :ios, '8.0'
  s.homepage     = 'http://www.synetech.cz'
  s.source       = { :git => 'https://bitbucket.org/synetech/oriflame-translations-ios.git', :tag => s.version.to_s }
  s.source_files  = 'ORLocalizedStrings/**/*.swift'
  s.requires_arc = true
  s.dependency  'Alamofire', '~> 3.5.0'
  s.dependency  'AFDateHelper', '~> 3.3.1'
  s.dependency  'ObjectMapper', '~> 1.2.0'
  s.dependency  'SYApiErrorReporting'
end