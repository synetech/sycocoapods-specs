Pod::Spec.new do |s|
  s.name             = 'SiteCoreNetworking'
  s.version          = '1.2.1'
  s.summary          = 'TODO: Add summary'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-sitecorenetworking-ios.git', :tag => s.version.to_s }
  s.social_media_url = 'synetech.cz'
  s.homepage         = 'synetech.cz'
  s.platform         = :ios, '10.0'
  s.requires_arc     = true
  s.default_subspec  = "Core"

  s.subspec "Core" do |ss|
    ss.source_files = "Sources/SiteCoreNetworking/**/*.swift"
    s.dependency 'Moya/RxSwift', '~> 14'
    s.dependency 'OriflameConfig'
    s.dependency "RxSwift"
  end
end
