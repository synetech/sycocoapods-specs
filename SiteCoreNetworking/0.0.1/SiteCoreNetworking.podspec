Pod::Spec.new do |s|
  s.name             = 'SiteCoreNetworking'
  s.version          = '0.0.1'
  s.summary          = 'A short description of initial screens'
  s.license          = "MIT"
  s.author           = { 'Filip Bulander' => 'filip.bulander@synetech.cz' }
  s.source           = { :git => 'git@bitbucket.org:synetech/oriflame-sitecorenetworking-ios.git', :tag => s.version.to_s }
  s.social_media_url = 'synetech.cz'
  s.homepage = 'synetech.cz'
  s.platform     = :ios, '9.0'
  s.source_files = 'Source/**/*.swift'  
  s.requires_arc = true
  s.dependency 'Config'
  s.dependency 'Moya'
end
