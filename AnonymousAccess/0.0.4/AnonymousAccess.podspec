Pod::Spec.new do |s|
  s.name              = "AnonymousAccess"
  s.version           = "0.0.4"
  s.summary           = "Anonymous access framework for Oriflame app."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/oriflame-anonymous-access-ios.git", :tag => s.version.to_s }
  s.resources         = ['Resources/*.xcassets', 'Resources/Fonts/*', 'Localized/*.lproj/*.strings']
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '5.0'

  s.dependency 'FontBlaster'
  s.dependency 'Result'
  s.dependency 'RxCocoa'
  s.dependency 'SteviaLayout'
  s.dependency 'SynevisionScanner'
  s.dependency 'Swinject'
  s.dependency 'SYNBase/Scalable'
  s.dependency 'Translations'
  s.dependency 'Kingfisher'
end
