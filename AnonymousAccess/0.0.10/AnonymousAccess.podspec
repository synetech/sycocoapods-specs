Pod::Spec.new do |s|
  s.name              = "AnonymousAccess"
  s.version           = "0.0.10"
  s.summary           = "Anonymous access framework for Oriflame app."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/oriflame-anonymous-access-ios.git", :tag => s.version.to_s }
  s.resources         = ['Resources/*.xcassets', 'Resources/Fonts/*', 'Localized/*.lproj/*.strings']
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'
  s.static_framework  = true

  s.dependency 'Apollo'
  s.dependency 'Apollo/SQLite'
  s.dependency 'Atributika'
  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/RemoteConfig'
  s.dependency 'FontBlaster'
  s.dependency 'Moya'
  s.dependency 'Result'
  s.dependency 'RxCocoa'
  s.dependency 'RxGesture'
  s.dependency 'RxDataSources'
  s.dependency 'SteviaLayout'
  s.dependency 'SynevisionScanner'
  s.dependency 'Swinject'
  s.dependency 'SYNBase/Loading'
  s.dependency 'SYNBase/Scalable'
  s.dependency 'SYNBase/Interactors/DatabaseStorage'
  s.dependency 'Translations'
  s.dependency 'Kingfisher', '~> 4.0'
  s.dependency 'ViewAnimator'
  s.dependency 'GDPerformanceView-Swift', '~> 2.0.2'
end
