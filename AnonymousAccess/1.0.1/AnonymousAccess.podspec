Pod::Spec.new do |s|
  s.name              = "AnonymousAccess"
  s.version           = "1.0.1"
  s.summary           = "Anonymous access framework for Oriflame app."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/oriflame-anonymous-access-ios.git", :tag => s.version.to_s }
  s.resources         = ['Resources/*.xcassets', 'Resources/Fonts/*', 'Localized/*.lproj/*.strings', 'Resources/RemoteConfigDefaults.plist']
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'
  s.static_framework  = true

  s.dependency 'Apollo', '0.14.0'
  s.dependency 'Apollo/SQLite', '0.14.0'
  s.dependency 'CommonNetworking', '0.0.3'
  s.dependency 'OriflameConfig'
  s.dependency 'Crashlytics'
  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/DynamicLinks'
  s.dependency 'Firebase/RemoteConfig'
  s.dependency 'FontBlaster'
  s.dependency 'Hero'
  s.dependency 'Moya'
  s.dependency 'RealmSwift', '3.21.0'
  s.dependency 'Result'
  s.dependency 'RxCocoa'
  s.dependency 'RxGesture'
  s.dependency 'RxDataSources'
  s.dependency 'SteviaLayout'
  s.dependency 'SynevisionScanner'
  s.dependency 'Swinject'
  s.dependency 'SYNBase/Loading'
  s.dependency 'SkeletonView'
  s.dependency 'SYNBase/Scalable'
  s.dependency 'SYNBase/Interactors/DatabaseStorage'
  s.dependency 'Translations'
  s.dependency 'Kingfisher', '~> 4.0'
  s.dependency 'GDPerformanceView-Swift', '~> 2.0.2'
end
