Pod::Spec.new do |s|
  s.name              = "NotificationList"
  s.version           = "0.4.3"
  s.summary           = "Notification list library."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/notification-list-ios.git", :tag => s.version.to_s }
  s.resource_bundles  = {'NotificationList' => ['Resources/*.xcassets', 'Resources/Fonts/*']}
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "10"
  s.source_files      = 'Sources/**/*.swift'
  s.exclude_files     = 'Sources/**/*Preview.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'
  s.static_framework  = true

  s.dependency 'FontBlaster'
  s.dependency 'Moya/RxSwift'
  s.dependency 'RxCocoa', "~> 5"
  s.dependency 'RxDataSources'
  s.dependency 'RxSwift', "~> 5"
  s.dependency 'SteviaLayout', '~> 4.7.3'
  s.dependency 'SYNBase/Interactors/DatabaseStorage', '>= 2.1.3'
  s.dependency 'Swinject'
  s.dependency 'SkeletonView', '1.8.5'
  s.dependency 'Kingfisher', '5.15.8'
end
