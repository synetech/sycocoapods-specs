Pod::Spec.new do |s|
  s.name              = "NotificationList"
  s.version           = "0.0.1"
  s.summary           = "Notification list library."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
	s.source            = { :git => "https://bitbucket.org/synetech/notification-list-ios.git", :tag => s.version.to_s }
  s.resource_bundles  = {'NotificationList' => ['Resources/*']}
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'
end
