Pod::Spec.new do |s|
  s.name              = "NotificationList"
  s.version           = "0.1.4"
  s.summary           = "Notification list library."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
	s.source            = { :git => "https://bitbucket.org/synetech/notification-list-ios.git", :tag => s.version.to_s }
  s.resources         = ['Resources/*.xcassets', 'Resources/Fonts/*']
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "9.0"
  s.source_files      = 'Sources/**/*.swift'
  s.requires_arc      = true
  s.swift_version     = '4.2'
  s.static_framework  = true

  s.dependency 'FontBlaster'
  s.dependency 'MarketingCloudSDK', '6.4'
  s.dependency 'Moya/RxSwift', '12.0.1'
  s.dependency 'OriflameConfig'
  s.dependency 'RxCocoa'
  s.dependency 'RxSwift'
  s.dependency 'SteviaLayout'
  s.dependency 'SYNBase/Interactors/DatabaseStorage'
  s.dependency 'Swinject'
  s.dependency 'SwipeCellKit'
end
