Pod::Spec.new do |s|
  s.name              = "DynamicScreens"
  s.version           = "1.0.0"
  s.summary           = "Synetech dynamic screens (Patent pending)"
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source            = { :git => "https://bitbucket.org/synetech/dynamic-screens-ios.git", :tag => s.version.to_s }
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "10.0"
  s.source_files      = 'Sources/**/*.swift'
  s.swift_version     = '4.2'

  s.dependency 'Kingfisher'
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'SteviaLayout'
  s.dependency 'Swinject'
end