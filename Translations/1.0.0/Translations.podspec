Pod::Spec.new do |s|
  s.name             = 'Translations'
  s.version          = '1.0.0'
  s.summary          = 'TODO: Add summary'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-translations-ios-v2.git', :tag => s.version.to_s }
  s.social_media_url = 'synetech.cz'
  s.homepage         = 'synetech.cz'
  s.platform         = :ios, '9.0'
  s.source_files     = 'Source/**/*.swift'  
  s.requires_arc     = true
  
  s.dependency 'Config'
  s.dependency 'Moya'
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'SiteCoreNetworking'
end
