Pod::Spec.new do |s|
  s.name             = 'Translations'
  s.version          = '2.2.0'
  s.summary          = 'TODO: Add summary'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-translations-ios-v2.git', :tag => s.version.to_s }
  s.social_media_url = 'synetech.cz'
  s.homepage         = 'synetech.cz'
  s.platform         = :ios, '10.0'
  s.source_files     = 'Sources/Translations/**/*.swift'
  s.requires_arc     = true

  s.dependency 'OriflameConfig'
  s.dependency 'Moya', '~> 14'
  s.dependency 'RxSwift'
  s.dependency 'SiteCoreNetworking'
end
