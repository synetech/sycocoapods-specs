Pod::Spec.new do |s|
  s.name             = 'SYNEMock'
  s.version          = '1.0.0'
  s.summary          = 'TODO: Add summary'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => 'https://gitlab.com/synetech/synetech-mock-ios', :tag => s.version.to_s }
  s.social_media_url = 'synetech.cz'
  s.homepage         = 'synetech.cz'
  s.platform         = :ios, '10.0'
  s.source_files     = 'Source/**/*.swift'
  s.requires_arc     = true

  s.dependency 'NetworkInterceptor'
end
