Pod::Spec.new do |s|
  s.name             = 'ORInitialScreens'
  s.version          = '3.1.15'
  s.summary          = 'A short description of initial screens'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-initial-screens-ios.git', :tag => s.version.to_s }
  s.resource_bundles = {'ORInitialScreens' => ['Resources/*']}
  s.resources        = ['Resources/Localizable/*.lproj/*.strings',  'Resources/Images/*.png']
  s.social_media_url = 'synetech.cz'
  s.homepage         = 'synetech.cz'
  s.platform         = :ios, '9.0'
  s.source_files     = 'Sources/**/*.swift'
  s.requires_arc     = true
  s.swift_version    = '4.2'

  s.dependency 'Alamofire'
  s.dependency 'SteviaLayout'
  s.dependency 'KeychainAccess'

  s.dependency 'OriflameConfig'
  s.dependency 'LoginNetworking', '~> 2.0.1'
  s.dependency 'Markets', '~> 2.0.1'
  s.dependency 'SiteCoreNetworking', '~> 1.1.2'
  s.dependency 'Translations', '~> 2.0.4'

  s.dependency 'RxSwift', '~> 4.0'
  s.dependency 'RxCocoa', '~> 4.0'

  s.dependency 'SYNBase/Extensions'
  s.dependency 'SYNBase/VIPER'
  s.dependency 'SYNBase/Routing'
end
