Pod::Spec.new do |s|
  s.name             = 'ORInitialScreens'
  s.version          = '2.0.1'
  s.summary          = 'A short description of initial screens'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-initial-screens-ios.git', :tag => s.version.to_s }
  s.resource_bundles = {'ORInitialScreens' => ['Resources/*']}
  s.resources        = ['Resources/Localizable/*.lproj/*.strings',  'Resources/Images/*.png']
  s.social_media_url = 'synetech.cz'
  s.homepage         = 'synetech.cz'
  s.platform         = :ios, '9.0'
  s.source_files     = 'Sources/**/*.swift'
  s.requires_arc     = true

  s.dependency 'Alamofire'
  s.dependency 'SteviaLayout'
  s.dependency 'KeychainAccess'

  s.dependency 'Config'
  s.dependency 'LoginNetworking/RxSwift'
  s.dependency 'Markets/RxSwift'
  s.dependency 'SiteCoreNetworking/RxSwift'
  s.dependency 'Translations'
  
  s.dependency 'SYNBase/Extensions'
  s.dependency 'SYNBase/VIPER'
  s.dependency 'SYNBase/Routing'
  s.dependency 'SYNBase/Interactors/DatabaseStorage'
end
