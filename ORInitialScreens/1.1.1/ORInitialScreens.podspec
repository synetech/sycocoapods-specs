Pod::Spec.new do |s|
  s.name             = 'ORInitialScreens'
  s.version          = '1.1.1'
  s.summary          = 'A short description of initial screens'
  s.license          = "MIT"
  s.author           = { 'Filip Bulander' => 'filip.bulander@synetech.cz' }
  s.source           = { :git => 'git@bitbucket.org:synetech/oriflame-initial-screens-ios.git', :tag => s.version.to_s }
  s.resource_bundles = {'ORInitialScreens' => ['Resources/*']}
  s.resources = ['Resources/Localizable/*.lproj/*.strings',  'Resources/Images/*.png']
  s.social_media_url = 'synetech.cz'
  s.homepage = 'synetech.cz'
  s.platform     = :ios, '9.0'
  s.source_files = 'ORInitialScreens/**/*.swift'  
  s.requires_arc = true
  s.dependency 'ORLocalizedStrings'
  s.dependency 'SteviaLayout', '~> 3.2'
  s.dependency 'KeychainAccess'
  s.dependency 'Alamofire'
  s.dependency 'SYNBase/Extensions', '~> 1.0.12'
end
