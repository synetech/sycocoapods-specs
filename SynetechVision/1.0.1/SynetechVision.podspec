Pod::Spec.new do |s|
  s.name         = "SynetechVision"
  s.version      = "1.0.1"
  s.summary      = "A base project that provides communication"
  s.license      = "MIT"
  s.author       = { "Filip Bulander" => "filip.bulander@synetech.cz" }
  
  s.description  = <<-DESC
                    This project provides base requests for Synetech Vision project.
                    DESC
  s.homepage     = "https://bitbucket.org/synetech/vision_ios_lib"
  
  s.source       = { :git => "git@bitbucket.org:synetech/vision_ios_lib.git", :tag => "#{s.version}" }
  s.source_files  = 'SynetechVision/**/*.swift'
  
  s.requires_arc = true
  s.platform     = :ios, "9.0"

  s.dependency 'Alamofire'
  s.dependency 'RxSwift', '~> 3.0'
  s.dependency 'Moya/RxSwift', '~> 8'
  s.dependency 'ObjectMapper'
end
