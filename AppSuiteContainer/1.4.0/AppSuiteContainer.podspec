Pod::Spec.new do |s|
  s.name             = 'AppSuiteContainer'
  s.version          = '1.4.0'
  s.summary          = 'ASC is an in-app module designated for Oriflame mobile apps. It shows a list of other branded apps with options to download/open and share'
  s.license          = "MIT"
  s.author           = { 'Lukas Ruzicka' => 'lukas.ruzicka@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-appsuite-ios.git', :tag => s.version.to_s }
  s.resource_bundles = {'AppSuiteContainer' => ['Resources/*.xcassets']}
  s.social_media_url = 'synetech.cz'
  s.homepage = 'synetech.cz'
  s.platform     = :ios, '10.0'
  s.source_files = 'Sources/*.swift', 'Sources/**/*.swift'
  s.resources = 'Resources/*'
  s.requires_arc = true
  s.static_framework  = true
  s.dependency 'Firebase/DynamicLinks'
  s.dependency 'Kingfisher'
  s.dependency 'Moya/RxSwift'
  s.dependency 'SteviaLayout'
  s.dependency 'SYNBase'
  s.dependency 'RealmSwift'
  s.dependency 'RxAlamofire'
  s.dependency 'RxCocoa'
  s.dependency 'RxSwift'
  s.dependency 'RxRealm'
end
