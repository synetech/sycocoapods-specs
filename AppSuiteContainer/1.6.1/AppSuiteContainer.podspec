Pod::Spec.new do |s|
  s.name             = 'AppSuiteContainer'
  s.version          = '1.6.1'
  s.summary          = 'ASC is an in-app module designated for Oriflame mobile apps. It shows a list of other branded apps with options to download/open and share'
  s.license          = "MIT"
  s.author           = { 'Lukas Ruzicka' => 'lukas.ruzicka@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-appsuite-ios.git', :tag => s.version.to_s }
  s.swift_version    = '4.2'

  s.social_media_url = 'synetech.cz'
  s.homepage = 'synetech.cz'
  s.platform     = :ios, '10.0'
  s.source_files = 'Sources/*.swift', 'Sources/**/*.swift'
  s.resource_bundles  = {'AppSuiteContainer' => ['Resources/*.xcassets', 'Resources/Fonts/*']}
  s.requires_arc = true
  s.static_framework  = true
  s.dependency 'Firebase/DynamicLinks'
  s.dependency 'Kingfisher'
  s.dependency 'Moya/RxSwift'
  s.dependency 'SteviaLayout', '~> 4.7.3'
  s.dependency 'Swinject'
  s.dependency 'SYNBase', '>= 2.1.3'
  s.dependency 'RealmSwift'
  s.dependency 'RxAlamofire'
  s.dependency 'RxCocoa', "~> 5"
  s.dependency 'RxSwift', "~> 5"
end
