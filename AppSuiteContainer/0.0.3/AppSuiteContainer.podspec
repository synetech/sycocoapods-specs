Pod::Spec.new do |s|
  s.name             = 'AppSuiteContainer'
  s.version          = '0.0.3'
  s.summary          = 'ASC is a in-app module (library), which will aggregate all the application owned by Oriflame company and display them to the user inside each of its application.'
  s.license          = "MIT"
  s.author           = { 'Filip Bulander' => 'filip.bulander@synetech.cz' }
  s.source           = { :git => 'git@bitbucket.org:synetech/oriflame-appsuite-ios.git', :tag => s.version.to_s }
  s.resource_bundles = {'AppSuiteContainer' => ['Resources/Images.xcassets']}
  s.social_media_url = 'synetech.cz'
  s.homepage = 'synetech.cz'
  s.platform     = :ios, '10.0'
  s.source_files = 'Sources/*.swift', 'Sources/**/*.swift', 'Resources/*.swift'
  s.requires_arc = true
  s.dependency 'Kingfisher'
  s.dependency 'Moya/RxSwift'
  s.dependency 'SteviaLayout', '~> 4.2.0'
  s.dependency 'SYNBase/Scalable'
  s.dependency 'RealmSwift'
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
  s.dependency 'RxRealm'
end
