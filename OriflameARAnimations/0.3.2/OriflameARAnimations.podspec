Pod::Spec.new do |s|
  s.name              = "OriflameARAnimations"
  s.version           = "0.3.2"
  s.summary           = "Library with AR animations for Oriflame app."
  s.license           = "EULA"
  s.author            = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
	s.source            = { :git => "https://bitbucket.org/synetech/oriflame-ar-animations-ios.git", :tag => s.version.to_s }
  s.social_media_url  = 'synetech.cz'
  s.homepage          = 'synetech.cz'
  s.platform          = :ios, "10.0"
  s.source_files      = 'Sources/**/*.swift'
  s.resources         = 'Resources/*'
  s.requires_arc      = true
  s.swift_version     = '4.2'

  s.dependency 'Alamofire', '~> 4.0'
  s.dependency 'RxCocoa'
  s.dependency 'RxSwift'
  s.dependency 'SteviaLayout'
  s.dependency 'Swinject'
end