Pod::Spec.new do |s|
  s.name             = "Markets"
  s.version          = '2.0.1'
  s.summary          = 'TODO: Add summary'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => "https://bitbucket.org/synetech/oriflame-markets-ios.git", :tag => s.version.to_s }
  s.social_media_url = "synetech.cz"
  s.homepage         = "synetech.cz"
  s.platform         = :ios, "9.0"
  s.requires_arc     = true
  s.default_subspec  = "Core"

  s.subspec "Core" do |ss|
    ss.source_files = "Source/**/*.swift"
    ss.dependency "OriflameConfig"
    ss.dependency 'Moya/RxSwift'
  end
end
