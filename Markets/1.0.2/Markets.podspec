Pod::Spec.new do |s|
  s.name             = "Markets"
  s.version          = '1.0.2'
  s.summary          = 'TODO: Add summary'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => "https://bitbucket.org/synetech/oriflame-markets-ios.git", :tag => s.version.to_s }
  s.social_media_url = "synetech.cz"
  s.homepage         = "synetech.cz"
  s.platform         = :ios, "9.0"
  s.requires_arc     = true
  s.default_subspec  = "Core"

  s.subspec "Core" do |ss|
      ss.source_files = "Source/**/*.swift"
      ss.exclude_files = "Source/Rx"
      ss.dependency "Config"
      ss.dependency "Moya"
      ss.framework  = "Foundation"
  end

  s.subspec "RxSwift" do |ss|
      ss.source_files = "Source/Rx/"
      ss.dependency "Markets/Core"
      ss.dependency "RxSwift"
  end

  # s.test_spec 'Tests' do |test_spec|
  #   test_spec.source_files = 'Tests/**/*.swift'
  #   test_spec.dependency "Markets/Core"
  #   test_spec.dependency "Markets/RxSwift"
  #   test_spec.dependency 'Quick'
  #   test_spec.dependency 'Nimble'
  # end
end
