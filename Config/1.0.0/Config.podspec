Pod::Spec.new do |s|
  s.name             = 'Config'
  s.version          = '1.0.0'
  s.summary          = 'TODO: Add summary'
  s.license          = "EULA"
  s.author           = { 'SYNETECH s.r.o.' => 'info@synetech.cz' }
  s.source           = { :git => 'https://bitbucket.org/synetech/oriflame-config-ios.git', :tag => s.version.to_s }
  s.social_media_url = 'synetech.cz'
  s.homepage         = 'synetech.cz'
  s.platform         = :ios, '9.0'
  s.source_files     = 'Source/**/*.swift'  
  s.requires_arc     = true

  s.test_spec 'Tests' do |test_spec|
    test_spec.source_files = 'Tests/**/*.swift'
    test_spec.dependency 'Quick' 
    test_spec.dependency 'Nimble'
  end
end
