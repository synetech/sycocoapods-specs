Pod::Spec.new do |s|
  s.name         = 'SYApiInteractor'
  s.version      = '1.0.0'
  s.summary      = 'A library that provides a universal interface to  API calls with Alamofire'
  s.license      = 'EULA'
  s.author             = { 'Daniel Rutkovský' => 'daniel.rutkovsky@synetech.cz' }
  s.social_media_url   = 'http://www.synetech.cz'
  s.platform     = :ios, '8.0'
  s.homepage     = 'http://www.synetech.cz'
  s.source       = { :git => 'https://bitbucket.org/synetech/syapiinteractor.git', :tag => s.version.to_s }
  s.source_files  = 'SYApiInteractor/**/*.swift'
  s.requires_arc = true
  s.dependency  'Alamofire', '~> 3.5.0'
  s.dependency  'ObjectMapper', '~> 1.2.0'
end